<article>
						<div class="post-slider">
							<div class="post-heading">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							</div>
							<!-- start flexslider -->
							<div id="post-slider" class="flexslider">
								<ul class="slides">
								
								<?php $slider_post = get_post_meta(get_the_ID(), 'gi', true ); 
									
									foreach ($slider_post as $post_slider ):
								?>
									<li>
									<img src="<?php echo $post_slider; ?>" alt="" />
									</li>
								<?php endforeach; ?>
								</ul>
							</div>
							<!-- end flexslider -->
						</div>
						<p>
							<?php echo wp_trim_words(get_the_content(), 40, false); ?>
						</p>
					<div class="bottom-article">
							<ul class="meta-post">
								<li><i class="icon-calendar"></i><a href="#"> <?php the_time('F g, Y'); ?></a></li>
								<li><i class="icon-user"></i><a href="#"><?php the_author(); ?></a></li>
								<li><i class="icon-folder-open"></i><a href="#"><?php the_category(); ?></a></li>
								<li><i class="icon-comments"></i><a href="#"><?php comments_popup_link('No comment', '1 comment', '% comments'); ?></a></li>
							</ul>
							<a href="<?php the_permalink(); ?>" class="pull-right">Continue reading <i class="icon-angle-right"></i></a>
						</div>
				</article>
<?php

	function theme_setup(){
		
		add_theme_support('title-tag');
		add_theme_support('post-thumbnails');
		add_theme_support('post-formats', array('video','audio', 'gallery'));
		add_theme_support('custom-header');
		add_theme_support('custom-background');
		add_theme_support('woocommerce');
		add_theme_support('widgets');
		add_theme_support('menus');
		
		
		// this is for slidebar text

		require_once('inc/widgets.php');
		
		
	}
	add_action('after_setup_theme', 'theme_setup');
	
// scripts are include here
		
		require_once('inc/scripts.php ');
		
// this is for custom post type		
		
		require_once('inc/custom_post.php');
		
// custom meta boxs		
		
		require_once('cmb/init.php');
		require_once('cmb/config.php');
		
// menus file

		require_once('inc/menus.php');
// add redux fremwork
		require_once('libs/ReduxCore/framework.php');	
		require_once('libs/sample/config.php');	
		
		

 
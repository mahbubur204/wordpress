<?php get_header(); ?>
	<!-- end header -->
	<section id="inner-headline">
	<?php echo get_template_part('title'); ?>
	</section>
	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
			
			<?php while(have_posts()): the_post(); ?>
				<article>
						<div class="post-image">
							<div class="post-heading">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							</div>
							<?php the_post_thumbnail(); ?>
						</div>
						<p>
							<?php the_content(); ?>
						</p>
						<div class="bottom-article">
							<ul class="meta-post">
								<li><i class="icon-calendar"></i><a href="#"> <?php the_time('F g, Y'); ?></a></li>
								<li><i class="icon-user"></i><a href="#"><?php the_author(); ?></a></li>
								<li><i class="icon-folder-open"></i><a href="#"><?php the_category(); ?></a></li>
								<li><i class="icon-comments"></i><a href="#"><?php comments_popup_link('No comment', '1 comment', '% comments'); ?></a></li>
							</ul>
							
						</div>
				</article>
			<?php endwhile; ?>	
				
				
				
				
				
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
	</section>
	<?php get_footer(); ?>
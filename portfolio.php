<?php 
/*
Template Name: Portfolio
*/
get_header(); ?>
	<!-- end header -->
	<section id="inner-headline">
	<?php echo get_template_part('title'); ?>
	</section>
	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="portfolio-categ filter">
					<li class="all active"><a href="#">All</a></li>
					<li class="web"><a href="#" title="">Web design</a></li>
					<li class="icon"><a href="#" title="">Icons</a></li>
					<li class="graphic"><a href="#" title="">Graphic design</a></li>
				</ul>
				<div class="clearfix">
				</div>
				<div class="row">
					<section id="projects">
					<ul id="thumbs" class="portfolio">
						<!-- Item Project and Filter Name -->
						
						<?php 
							
							$port = new WP_Query(array(
								
								'post_type'		 => 'portfolio',
								'posts_per_page' => 8,
								
							));
						
						while($port->have_posts()): $port->the_post(); ?>
						<li class="item-thumbs col-lg-3 design" data-id="id-0" data-type="web">
						<!-- Fancybox - Gallery Enabled - Title - Full Image -->
						<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Portfolio name" href="img/works/1.jpg">
						<span class="overlay-img"></span>
						<span class="overlay-img-thumb font-icon-plus"></span>
						</a>
						<!-- Thumb Image and Description -->
						<?php the_post_thumbnail(); ?>
						</li>
						<?php endwhile; ?>
						
					</ul>
					</section>
				</div>
			</div>
		</div>
	</div>
	</section>
	<?php get_footer(); ?>
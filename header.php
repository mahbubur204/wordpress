<?php global $redux_demo; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="http://bootstraptaste.com" />


<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper">
	<!-- start header -->
	<header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><span><?php echo $redux_demo['logoone']; ?></span><?php echo $redux_demo['logotwo']; ?></a>
                </div>
                <div class="navbar-collapse collapse ">
				
				
				<?php wp_nav_menu(array(
						'menu_location'		=> 'main_menu',
						'menu_class'		=> 'nav navbar-nav',
						'fallback_cb'		=> 'theme_menu',
						'menu_id'			=> ' ',
						'container'	=> ' ',
						
				
				) ); ?>
                   
                </div>
            </div>
        </div>
	</header>
<?php global $redux_demo; ?>

<footer>
	<div class="container">
		<div class="row">
		<?php dynamic_sidebar('fber'); ?>
			<!--
			<div class="col-lg-3">
				<div class="widget">
					<h5 class="widgetheading">Flickr photostream</h5>
					<div class="flickr_badge">
						<script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=8&amp;display=random&amp;size=s&amp;layout=x&amp;source=user&amp;user=34178660@N03"></script>
					</div>
					<div class="clear">
					</div>
				</div>
			</div>-->
		</div>
	</div>
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="copyright">
						<p>
						
						<?php echo  $redux_demo['cpt']; ?>
							
						</p>
					</div>
				</div>
				<div class="col-lg-6">
					<ul class="social-network">
					
						<?php if(esc_url($redux_demo['fb'])): ?>
						<li><a href="<?php echo esc_url($redux_demo['fb']);?>" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<?php endif; ?>
						
						<?php if(esc_url($redux_demo['tw'])): ?>
						<li><a href="<?php echo esc_url($redux_demo['tw']);?>" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
						<?php endif; ?>
						
						<?php if(esc_url($redux_demo['li'])): ?>
						<li><a href="<?php echo esc_url($redux_demo['li']);?>" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
						<?php endif; ?>
						
						<?php if(esc_url($redux_demo['pt'])): ?>
						<li><a href="<?php echo esc_url($redux_demo['pt']);?>" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
						<?php endif; ?>
						
						<?php if(esc_url($redux_demo['gp'])): ?>
						<li><a href="<?php echo esc_url($redux_demo['gp']);?>" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	</footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->



<?php wp_footer(); ?>
</body>
</html>
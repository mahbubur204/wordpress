<?php get_header(); ?>
	<!-- end header -->
	<section id="inner-headline">
	<?php echo get_template_part('title'); ?>
	</section>
	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
			
			<?php while(have_posts()): the_post(); ?>
				<?php echo get_template_part('templates/content', get_post_format()); ?>
			<?php endwhile; ?>	
				
				
				
				
				<ul class="pagination">
					<?php the_posts_pagination(array(
						'prev_text'	=> 'PREV',
						'Next'		=> 'Next',
						'screen_reader_text' => ' ',
						'type'		=> 'list',
				)); ?>
					
					
				</ul>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
	</section>
	<?php get_footer(); ?>
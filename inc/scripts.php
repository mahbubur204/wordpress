<?php


	function theme_style_scripts(){
		
		wp_enqueue_style('bootstrap',get_template_directory_uri().'/css/bootstrap.min.css');
		wp_enqueue_style('fancybox',get_template_directory_uri().'/css/fancybox/jquery.fancybox.css');
		wp_enqueue_style('jcarousel',get_template_directory_uri().'/css/jcarousel.css');
		wp_enqueue_style('flexslider',get_template_directory_uri().'/css/flexslider.css');
		wp_enqueue_style('style',get_template_directory_uri().'/css/style.css');
		wp_enqueue_style('default',get_template_directory_uri().'/skins/default.css');
		wp_enqueue_style('stylesheet',get_stylesheet_uri());
		
		
		wp_enqueue_script('easing',get_template_directory_uri().'/js/jquery.easing.1.3.js',array('jquery'),true, true);
		wp_enqueue_script('bootstrapjs',get_template_directory_uri().'/js/bootstrap.min.js',array('jquery'),true, true);
		wp_enqueue_script('fancybox',get_template_directory_uri().'/js/jquery.fancybox.pack.js',array('jquery'),true, true);
		wp_enqueue_script('fancybox-media',get_template_directory_uri().'/js/jquery.fancybox-media.js',array('jquery'),true, true);
		wp_enqueue_script('prettify',get_template_directory_uri().'/js/google-code-prettify/prettify.js',array('jquery'),true, true);
		wp_enqueue_script('quicksand',get_template_directory_uri().'/js/portfolio/jquery.quicksand.js',array('jquery'),true, true);
		wp_enqueue_script('setting',get_template_directory_uri().'/js/portfolio/setting.js',array('jquery'),true, true);
		wp_enqueue_script('flexslider',get_template_directory_uri().'/js/jquery.flexslider.js',array('jquery'),true, true);
		wp_enqueue_script('animate',get_template_directory_uri().'/js/animate.js',array('jquery'),true, true);
		wp_enqueue_script('custom',get_template_directory_uri().'/js/custom.js',array('jquery'),true, true);
		
	}
	add_action('wp_enqueue_scripts', 'theme_style_scripts');
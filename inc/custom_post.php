<?php 

	//slider post
	
	register_post_type('slider', array(
			
			'public'	=> true,
			'labels'	=> array(
				'name'				=> 'slider',
				'add_new'			=> 'Add new Slide',
				'all_itmes'			=> 'All sliders',
				'add_new_item'		=> 'Add new Slide',
				'featured_image'	=> 'Add slider image',
				'set_featured_image'	=> 'Add slider image',
			),
			'supports'		=> array('title', 'editor', 'thumbnail'),		
	
	
	));
	
	register_post_type('servise', array(
			
			'public'	=> true,
			'labels'	=> array(
				'name'				=> 'servise',
				'add_new'			=> 'Add new servise',
				'all_itmes'			=> 'All servises',
				'add_new_item'		=> 'Add new servises',
				'featured_image'	=> 'Add servises image',
				'set_featured_image'	=> 'Add servises image',
			),
			'supports'		=> array('title', 'editor'),		
	
	
	));
	
	register_post_type('portfolio', array(
			
			'public'	=> true,
			'labels'	=> array(
				'name'				=> 'portfolio',
				'add_new'			=> 'Add new portfolio',
				'all_itmes'			=> 'All portfolios',
				'add_new_item'		=> 'Add new portfolio',
				'featured_image'	=> 'Add portfolio image',
				'set_featured_image'	=> 'Add portfolio image',
			),
			'supports'		=> array('title', 'editor', 'thumbnail'),		
	
	
	));
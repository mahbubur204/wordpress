<?php 

//this is for register. sideber

register_sidebar(array(
	'name'			=> 'Right Sidebar',
	'id'			=> 'right_bar',
	'description'	=> 'Input element',
	'before_widget'		=> '<div class="widget">',
	'after_widget'		=> '</div>',
	'before_title'		=> '<h5 class="widgetheading">',
	'after_title'		=> '</h5>',
) );

register_sidebar(array(
	'name'			=> 'Footer Sidebar',
	'id'			=> 'fber',
	'description'	=> 'Footer sideber',
	'before_widget' => '<div class="col-lg-3"><div class="widget">',
	'after_widget'  => '</div></div>',
	'before_title'  => '<h5 class="widgetheading">',
	'after_title'  => '</h5>',
	
));


//custom widget development 
		
		class r_post extends WP_Widget{
			
			public function __construct (){
				parent :: __construct('r_post', 'Resent post thumbnail', array(
					
					'description'		=> 'Resent post with thumbnail',
					
				) );
			}
			
			public function widget($one, $two ){?>
			
			<?php echo $one['before_widget']; ?>
			
		<?php echo $one['before_title']; ?>Latest posts<?php echo $one['after_title']; ?>
			<ul class="recent">
				<?php 
				
				$wposts = new WP_Query(array(
						'post_type'		=> 'post',
						'posts_per_page'	=> 4,
				));
				
				while($wposts->have_posts()): $wposts->the_post(); ?>
				
				
						<li>
						<?php the_post_thumbnail(); ?>
						<h6><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
						<p>
							<?php echo wp_trim_words(get_the_content(), 8, false); ?>
						</p>
						</li>
						
				<?php endwhile; ?>	
			</ul>		
			
			<?php echo $one['after_widget']; ?>
				
			<?php }
			
		}
	



function theme_wedgets_dev(){
	register_widget('r_post');
} 

add_action('widgets_init', 'theme_wedgets_dev');